﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using ArrivalTimesViewer.Dto;
using FluentAssertions;
using Newtonsoft.Json;
using Xunit;

namespace ArrivalTimesViewer.Test.Controllers
{
    public class RouteControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly CustomWebApplicationFactory<Startup> _factory;

        public RouteControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task NumberOfRoutesIs3()
        {
            var client = _factory.CreateClient();

            HttpResponseMessage routesResponse = await client.GetAsync("/api/route");

            routesResponse.EnsureSuccessStatusCode();

            var stringResponse = await routesResponse.Content.ReadAsStringAsync();
            var routes = JsonConvert.DeserializeObject<ICollection<RouteDto>>(stringResponse);
            routes.Should().NotBeNullOrEmpty()
                .And.HaveCount(3);

            routes.Select(stop => stop.Id).OrderBy(id => id).Should()
                .ContainInOrder(new int[] { 0,1, 2 });
        }
    }
}
