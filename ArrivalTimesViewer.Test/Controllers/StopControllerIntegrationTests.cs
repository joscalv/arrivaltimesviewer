﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using ArrivalTimesViewer.Dto;
using Newtonsoft.Json;
using Xunit;
using FluentAssertions;
using System.Linq;

namespace ArrivalTimesViewer.Test.Controllers
{
    public class StopControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly CustomWebApplicationFactory<Startup> _factory;

        public StopControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task NumberOfStopsIs10()
        {
            var client = _factory.CreateClient();

            HttpResponseMessage stopsResponse = await client.GetAsync("/api/stop");

            stopsResponse.EnsureSuccessStatusCode();

            var stringResponse = await stopsResponse.Content.ReadAsStringAsync();
            var stops = JsonConvert.DeserializeObject<ICollection<StopDto>>(stringResponse);
            stops.Should().NotBeNullOrEmpty()
                .And.HaveCount(10);

            stops.Select(stop => stop.Id).OrderBy(id => id).Should()
                .ContainInOrder(new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9});
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        [InlineData(4)]
        [InlineData(5)]
        [InlineData(6)]
        [InlineData(7)]
        [InlineData(8)]
        [InlineData(9)]
        public async Task RetreiveStop(int stopId)
        {
            var client = _factory.CreateClient();

            HttpResponseMessage stopsResponse = await client.GetAsync($"/api/stop/{stopId}");

            stopsResponse.EnsureSuccessStatusCode();

            var stringResponse = await stopsResponse.Content.ReadAsStringAsync();
            var stop = JsonConvert.DeserializeObject<StopDto>(stringResponse);

            stop.Should().NotBeNull();
            stop.Id.Should().Be(stopId);
        }

    }
}
