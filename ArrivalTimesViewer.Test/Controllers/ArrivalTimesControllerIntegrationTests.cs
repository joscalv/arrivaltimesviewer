﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using ArrivalTimesViewer.Dto;
using FluentAssertions;
using Newtonsoft.Json;
using Xunit;

namespace ArrivalTimesViewer.Test.Controllers
{
    public class ArrivalTimesControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly CustomWebApplicationFactory<Startup> _factory;

        public ArrivalTimesControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task EachStopIsServedByThreeRoutes()
        {
            int expectedNumberOfStops = 10;
            int expectedNumerOfRoutes = 3;

            var client = _factory.CreateClient();

            var arrivalTimes = await RequestArrivalTimes(client);

            arrivalTimes.Should().NotBeNullOrEmpty()
                .And.HaveCount(expectedNumberOfStops * expectedNumerOfRoutes);

        }

        [Fact]
        public async Task ThereIsAlwaysTwoEtasPerStopAndRoute()
        {
            int expectedEtasPerStopAndRoute = 2;

            var client = _factory.CreateClient();

            var arrivalTimes = await RequestArrivalTimes(client);

            foreach (var arrivalTimeDto in arrivalTimes)
            {
                arrivalTimeDto.Etas.Should().NotBeNullOrEmpty()
                    .And.HaveCount(expectedEtasPerStopAndRoute);
            }

        }

        [Fact]
        public async Task EveryRouteServesEveryStopWithFrecuency15()
        {
            int expectedFrecuency = 15;

            var client = _factory.CreateClient();

            var arrivalTimes = await RequestArrivalTimes(client);

            foreach (var arrivalTimeDto in arrivalTimes)
            {
                (arrivalTimeDto.Etas.ElementAt(1) - arrivalTimeDto.Etas.ElementAt(0)).Should().Be(expectedFrecuency);

            }

        }

        private static async Task<ICollection<ArrivalTimeDto>> RequestArrivalTimes(HttpClient client)
        {
            HttpResponseMessage routesResponse = await client.GetAsync("/api/arrivaltime");

            routesResponse.EnsureSuccessStatusCode();

            var stringResponse = await routesResponse.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<ICollection<ArrivalTimeDto>>(stringResponse);


        }

        private static async Task<ICollection<ArrivalTimeDto>> RequestArrivalTimes(HttpClient client, int stopId)
        {
            HttpResponseMessage routesResponse = await client.GetAsync($"/api/stop/{stopId}/arrivaltime/");

            routesResponse.EnsureSuccessStatusCode();

            var stringResponse = await routesResponse.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<ICollection<ArrivalTimeDto>>(stringResponse);


        }
    }
}
