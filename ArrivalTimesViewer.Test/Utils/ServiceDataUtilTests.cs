using System;
using System.Linq;
using ArrivalTimesViewer.Utils;
using FluentAssertions;
using Microsoft.AspNetCore.Server.Kestrel.Core.Internal.Http2;
using Xunit;

namespace ArrivalTimesViewer.Test
{
    public class ServiceDataUtilTest
    {
        [Fact]
        public void ThereAre10Stops()
        {
            var stops = ServiceDataUtil.Stops;
            stops.Should().HaveCount(10);
            stops.Select(stop => stop.Id).OrderBy(id => id).Should()
                .ContainInOrder(new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
        }

        [Fact]
        public void ThereAre3Routes()
        {
            var routes = ServiceDataUtil.Routes;
            routes.Should().HaveCount(3);
            routes.Select(route => route.Id).OrderBy(id => id).Should()
                .ContainInOrder(new int[] { 0, 1, 2 });
        }

        [Theory]
        [InlineData(15, 1, 0, 0, 14, 29)]
        [InlineData(15, 1, 0, 1, 1, 16)]
        [InlineData(15, 1, 0, 2, 3, 18)]
        [InlineData(15, 1, 1, 0, 1, 16)]
        [InlineData(15, 1, 1, 1, 3, 18)]
        [InlineData(15, 1, 1, 2, 5, 20)]
        
        public void EachStopIsServicedEvery15MinPerRoute(int hour, int minute, int stop, int route, int eta1, int eta2)
        {
            var date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, hour, minute, 0);
            var etas = ServiceDataUtil.CalculateArrivalTimes(date);
            etas.First(eta => eta.StopId == stop && eta.RouteId == route).Etas.First().Should().Be(eta1);
            etas.First(eta => eta.StopId == stop && eta.RouteId == route).Etas.ElementAt(1).Should().Be(eta2);
        }
    }
}
