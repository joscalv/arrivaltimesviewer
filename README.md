# ArrivalTimesViewer

To start the project it is necesary execute this commands:

1. dotnet publish (this is necesary to generate the client app)
2. dotnet run

Using a development environment, .net core and SPA midleware will handle the creation of the client app.  

The project will execute in ports 5000 and 5001 for http and https respectively.  
Swagger documentation is in: http://localhost:5000/swagger/index.html

The depencies of the project and the technologies in which it has been developed are:

- .net Core 2.2  
- nodejs 8 
- yarn

This project has been developed mainly following this steps:  

1. Create the project structure using the template of visual studio of react, typescript and .net Core.
2. Replace the client app by a newer one, created using create-react-app utility.
3. Configure the project and targets to create the api clients using swagger and nswag. In this way, the api clients are created in each build.
4. Add material-ui to develop the UI.
5. Add redux and thunk to manage the internal state and connect the application with the Api.
6. Add signalr to handle the asyncronous updates.
6. Develop de frontend and backend, creating unit test.

