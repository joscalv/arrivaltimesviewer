﻿namespace ArrivalTimesViewer.Dto
{
    public class RouteDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
