﻿using System.Collections.Generic;

namespace ArrivalTimesViewer.Dto
{
    public class ArrivalTimeDto
    {
        public int StopId { get; set; }
        public int RouteId { get; set; }
        public IEnumerable<int> Etas { get; set; }
    }
}
