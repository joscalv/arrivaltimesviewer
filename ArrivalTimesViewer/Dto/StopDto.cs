﻿using Microsoft.EntityFrameworkCore.Storage;

namespace ArrivalTimesViewer.Dto
{
    public class StopDto
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string Description { get; set; }

        public Point Position { get; set; }
    }

    public class Point
    {
        public double X { get; set; }
        public double Y { get; set; }
    }
}
