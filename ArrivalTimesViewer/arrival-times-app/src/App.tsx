import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import React from 'react';
import { Provider } from 'react-redux';
import styles from './App.module.css';
import ArrivalTimesViewer from './components/arrivalTimesViewer/arrivalTimesViewer';
import store from './store/store';

const theme = createMuiTheme({ spacing: 4 });

const App: React.FC = () => {
  return (
    <div className={styles.background}>
      <div className={styles.appContainer}>
        <Provider store={store}>
          <MuiThemeProvider theme={theme}>
            <ArrivalTimesViewer />
          </MuiThemeProvider>
        </Provider>
      </div>
    </div>
  );
};

export default App;
