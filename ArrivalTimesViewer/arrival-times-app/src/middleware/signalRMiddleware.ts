import { HubConnection, HubConnectionBuilder, LogLevel } from '@aspnet/signalr';
import { Action, Dispatch, MiddlewareAPI } from 'redux';
import { UpdateArrivalTimes } from '../reducers/arrivalTimes';
import { ActionTypes, connectedToHub, disconnectToHub } from '../reducers/connection';
import { ArrivalTime } from '../services/ArrivalTimeService';

let connection: HubConnection;

const signalRMiddleware = () => {
  return (storeAPI: MiddlewareAPI) => (next: Dispatch) => (action: Action) => {
    if (!connection) {
      connection = new HubConnectionBuilder()
        .withUrl("/api/arrivaltimeshub")
        .configureLogging(LogLevel.Information)
        .build();
    }
    switch (action.type) {
      case ActionTypes.CONNECT: {
        startConnection(storeAPI);
        subscribe(storeAPI);
        break;
      }
      case ActionTypes.DISCONNECT: {
        closeConnection(storeAPI);
        break;
      }
    }

    return next(action);
  };
};

function startConnection(storeAPI: MiddlewareAPI): void {
  console.log("Connecting to hub");
  connection
    .start()
    .then(() => {
      console.log("Hub connected");

      storeAPI.dispatch(connectedToHub());
    })
    .catch((err: Error) => {
      console.log("Error connecting to hub");
    });
}

function closeConnection(storeAPI: MiddlewareAPI): void {
  console.log("Closing connection to hub...");
  connection
    .stop()
    .then(() => {
      console.log("Hub Connection closed");
      storeAPI.dispatch(disconnectToHub());
    })
    .catch(() => {
      console.log("Error closing connection to hub");
    });
}

function subscribe(storeAPI: MiddlewareAPI): void {
  connection.on("UpdateArrivalTimes", (etas: ArrivalTime[]) => {
    storeAPI.dispatch(UpdateArrivalTimes(etas));
  });
}

export default signalRMiddleware;
