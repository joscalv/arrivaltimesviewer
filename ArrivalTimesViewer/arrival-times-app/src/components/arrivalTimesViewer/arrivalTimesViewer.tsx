import { Typography } from '@material-ui/core';
import React from 'react';
import { connect } from 'react-redux';
import { getAllArrivalTimes } from '../../reducers/arrivalTimes';
import { connectToHub, disconnectToHub } from '../../reducers/connection';
import { State } from '../../reducers/index';
import { getAllRoutes } from '../../reducers/routes';
import { getAllStops } from '../../reducers/stops';
import { ArrivalTime, Route, Stop } from '../../services/ArrivalTimeService';
import StopViewer from '../common/stop/stopViewer';
import styles from './arrivalTimesViewer.module.css';

interface ArrivalTimesViewerState {
  secondsSinceLastUpdate?: number;
}

interface ArrivalTimesViewerStateProps {
  stops: Stop[];
  routes: Route[];
  arrivalTimes: ArrivalTime[];
  lastUpdate?: Date;
  fechingStops: boolean;
  fetchingRoutes: boolean;
  fetchingArrivalTimes: boolean;
}

interface ArrivalTimesViewerActionProps {
  getAllStops: typeof getAllStops;
  getAllRoutes: typeof getAllRoutes;
  getAllArrivalTimes: typeof getAllArrivalTimes;
  connectToHub: typeof connectToHub;
  disConnectToHub: typeof disconnectToHub;
}

type ArrivalTimesViewerProps = ArrivalTimesViewerStateProps &
  ArrivalTimesViewerActionProps;

class ArrivalTimesViewer extends React.Component<
  ArrivalTimesViewerProps,
  ArrivalTimesViewerState
> {
  state: Readonly<ArrivalTimesViewerState> = {
    secondsSinceLastUpdate: undefined
  };

  componentDidMount() {
    this.props.getAllStops();
    this.props.getAllRoutes();
    this.props.getAllArrivalTimes();
    this.props.connectToHub();
    setInterval(() => {
      if (this.props.lastUpdate) {
        this.setState({
          secondsSinceLastUpdate: Math.trunc(
            (new Date().getTime() - this.props.lastUpdate.getTime()) / 1000
          )
        });
      }
    }, 1000);
  }

  renderStopViewer(stop: Stop, routes: Route[], arrivalTimes: ArrivalTime[]) {
    let arrivalTimesOfStop = arrivalTimes.filter(eta => eta.stopId === stop.id);
    return (
      <StopViewer
        key={stop.id}
        stop={stop}
        routes={this.props.routes}
        arrivalTimes={arrivalTimesOfStop}
      />
    );
  }

  render() {
    let isfeching =
      this.props.fechingStops ||
      this.props.fetchingRoutes ||
      this.props.fetchingArrivalTimes;

    let stopViews = this.props.stops.map((stop: Stop) =>
      this.renderStopViewer(stop, this.props.routes, this.props.arrivalTimes)
    );

    return (
      <React.Fragment>
        {isfeching && "loading"}
        {!isfeching && (
          <React.Fragment>
            <Typography variant="h1" component="h2" gutterBottom={true} className={styles.title}>
              Arrival Times
            </Typography>
            <Typography variant="caption" display="block" gutterBottom={true} className={styles.title}>
              Last update:{" "}
              {this.state.secondsSinceLastUpdate!== undefined
                ? this.state.secondsSinceLastUpdate.toString() + " seconds ago"
                : "never"}
            </Typography>
            {stopViews}
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

function mapStateToProps(state: State): ArrivalTimesViewerStateProps {
  return {
    stops: state.stops.stops,
    routes: state.routes.routes,
    arrivalTimes: state.arrivalTimes.arrivalTimes,
    lastUpdate: state.arrivalTimes.updatedDate,
    fechingStops: state.stops.isFeching,
    fetchingRoutes: state.routes.isFeching,
    fetchingArrivalTimes: state.arrivalTimes.isFeching
  };
}

function mapDispatchToProps(
  dispatch: (action: any) => any
): ArrivalTimesViewerActionProps {
  return {
    getAllStops: () => dispatch(getAllStops()),
    getAllRoutes: () => dispatch(getAllRoutes()),
    getAllArrivalTimes: () => dispatch(getAllArrivalTimes()),
    connectToHub: () => dispatch(connectToHub()),
    disConnectToHub: () => dispatch(disconnectToHub())
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ArrivalTimesViewer);
