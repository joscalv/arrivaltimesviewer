import {
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  Typography
  } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import * as React from 'react';
import { ArrivalTime, Route, Stop } from '../../../services/ArrivalTimeService';
import RouteArrivalTimes from '../routeArrivalTimes/routeArrivalTimes';
import classes from './stopViewer.module.css';

interface StopViewerProps {
  stop: Stop;
  routes: Route[];
  arrivalTimes: ArrivalTime[];
}

const StopViewer: React.SFC<StopViewerProps> = props => {
  return (
    <React.Fragment>
      <ExpansionPanel className={classes.root}>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography className={classes.heading}>{props.stop.name}</Typography>
          <Typography className={classes.secondaryHeading}>
            {props.stop.description}
          </Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <div className={classes.routesContainer}>
            {props.routes.map(r => {
              let eta = props.arrivalTimes.find(eta => eta.routeId === r.id);

              return (
                <RouteArrivalTimes key={r.id} route={r} arrivalTime={eta} />
              );
            })}
          </div>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    </React.Fragment>
  );
};

export default StopViewer;
