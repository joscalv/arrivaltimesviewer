import { Typography } from '@material-ui/core';
import * as React from 'react';
import styles from './arrivalTimeText.module.css';

interface ArrivalTimeTextProps {
  minutes: number;
}

const ArrivalTimeText: React.SFC<ArrivalTimeTextProps> = props => {
  return (
    <Typography className={(props.minutes >=3 ? styles.text : styles.inminentText)}>
      {formatMinutes(props.minutes)}
    </Typography>
  );
};

const formatMinutes = (minutes: number) =>
  minutes.toString().padStart(2) + " min.";

export default ArrivalTimeText;
