import { Typography } from '@material-ui/core';
import * as React from 'react';
import { ArrivalTime, Route } from '../../../services/ArrivalTimeService';
import ArrivalTimeText from '../arrivalTimeText/arrivalTimeText';
import styles from './routeArrivalTimes.module.css';

interface RouteArrivalTimesProps {
  route: Route;
  arrivalTime?: ArrivalTime;
}

const RouteArrivalTimes: React.SFC<RouteArrivalTimesProps> = props => {
  return (
    <div className={styles.root}>
      <Typography className={styles.text}>{props.route.name}</Typography>
      {props.arrivalTime && props.arrivalTime.etas && props.arrivalTime.etas.map((eta, index)=> <ArrivalTimeText key={index} minutes={eta}/>
      )}
    </div>
  );
};




export default RouteArrivalTimes;
