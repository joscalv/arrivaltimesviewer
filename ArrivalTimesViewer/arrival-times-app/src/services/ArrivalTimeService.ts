import { ArrivalTimesClient, StopDto, RouteDto, ArrivalTimeDto } from './restClient/ArrivalTimesClient';

export type Stop = StopDto;
export type Route = RouteDto;
export type ArrivalTime = ArrivalTimeDto;

export class ArrivalTimeService {
    private client: ArrivalTimesClient;

    public constructor() {
        this.client = new ArrivalTimesClient(); ;
    }

    getAllStops(): Promise<Stop[]> {
        return this.client.getStops();
    }

    getAllRoutes(): Promise<Route[]> {
        return this.client.getRoutes();
    }

    getAllArrivalTimes(): Promise<ArrivalTime[]> {
        return this.client.getArrivalTimes();
    }

}
