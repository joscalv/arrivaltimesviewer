import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import signalRMiddleware from '../middleware/signalRMiddleware';
import { initialState, reducer } from '../reducers';

// We'll be using Redux Devtools. We can use the `composeWithDevTools()`
// directive so we can pass our middleware along with it

/*
 * We're giving State interface to create store
 * store is type of State defined in our reducers
 */
const composeEnhancers = composeWithDevTools({});


const store = createStore(
  reducer,
  initialState,
  composeEnhancers(applyMiddleware(logger, thunk, signalRMiddleware()))
);

export default store;
