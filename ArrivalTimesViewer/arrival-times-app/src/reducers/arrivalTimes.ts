import { AnyAction } from 'redux';
import { ThunkAction, ThunkDispatch } from 'redux-thunk';
import { ArrivalTime, ArrivalTimeService } from '../services/ArrivalTimeService';

export enum ActionTypes {
  ADD_ALL_ETAS = "ADD_ALL_ETAS",
  UPDATE_ETAS = "UPDATE_ETAS",
  ETAS_REQUEST_STATUS = "ETAS_REQUEST_STATUS",
  ADD_ERROR_FETCHING_ETAS = "ADD_ERROR_FETCHING_ETAS"
}

export interface AddAllEtasAction {
  type: ActionTypes.ADD_ALL_ETAS;
  payload: { arrivalTimes: ArrivalTime[] };
}

export interface UpdateArrivalTimesAction {
  type: ActionTypes.UPDATE_ETAS;
  payload: { arrivalTimes: ArrivalTime[] };
}
export interface EtasRequestStatusAction {
  type: ActionTypes.ETAS_REQUEST_STATUS;
  payload: { isFeching: boolean };
}
export interface AddErrorFetchingEtasAction {
  type: ActionTypes.ADD_ERROR_FETCHING_ETAS;
  payload: { isError: boolean; message: string };
}

export function AddAllEtas(etas: ArrivalTime[]): AddAllEtasAction {
  return {
    type: ActionTypes.ADD_ALL_ETAS,
    payload: { arrivalTimes: etas }
  };
}

export function UpdateArrivalTimes(
  etas: ArrivalTime[]
): UpdateArrivalTimesAction {
  return {
    type: ActionTypes.UPDATE_ETAS,
    payload: { arrivalTimes: etas }
  };
}

export function StartEtasRequestStatus(): EtasRequestStatusAction {
  return {
    type: ActionTypes.ETAS_REQUEST_STATUS,
    payload: { isFeching: true }
  };
}

export function EndEtasRequestStatus(): EtasRequestStatusAction {
  return {
    type: ActionTypes.ETAS_REQUEST_STATUS,
    payload: { isFeching: false }
  };
}

export function AddErrorOnEtasRequest(
  isError: boolean,
  message: string
): AddErrorFetchingEtasAction {
  return {
    type: ActionTypes.ADD_ERROR_FETCHING_ETAS,
    payload: { isError: isError, message: message }
  };
}

export type Action =
  | AddAllEtasAction
  | UpdateArrivalTimesAction
  | EtasRequestStatusAction
  | AddErrorFetchingEtasAction;

export interface State {
  arrivalTimes: ArrivalTime[];
  isFeching: boolean;
  isErrorFetching: boolean;
  errorMessage: string;
  updatedDate?: Date;
}

export const initialState: State = {
  arrivalTimes: [],
  isFeching: false,
  isErrorFetching: false,
  errorMessage: "",
  updatedDate: undefined
};

export function reducer(state: State = initialState, action: Action): State {
  switch (action.type) {
    case ActionTypes.ADD_ALL_ETAS: {
      return {
        ...state,
        updatedDate: new Date(),
        arrivalTimes: action.payload.arrivalTimes
      };
    }
    case ActionTypes.UPDATE_ETAS: {
      let etas = action.payload.arrivalTimes;
      var oldEtas = state.arrivalTimes;
      var newEtas: ArrivalTime[] = [];
      newEtas = [...etas];
      oldEtas.forEach(oldEta => {
        if (
          !newEtas.find(
            e => e.routeId === oldEta.routeId && e.stopId === oldEta.stopId
          )
        ) {
          newEtas.push(oldEta);
        }
      });

      return { ...state, updatedDate: new Date(),arrivalTimes: newEtas };
    }
    case ActionTypes.ETAS_REQUEST_STATUS: {
      return {
        ...state,
        isFeching: action.payload.isFeching
      };
    }
    case ActionTypes.ADD_ERROR_FETCHING_ETAS: {
      return {
        ...state,
        isErrorFetching: action.payload.isError,
        errorMessage: action.payload.message
      };
    }
    default: {
      return state;
    }
  }
}

const client = new ArrivalTimeService();

export function getAllArrivalTimes(): ThunkAction<
  Promise<void>,
  {},
  {},
  AnyAction
> {
  return (dispatch: ThunkDispatch<{}, {}, AnyAction>): Promise<void> => {
    dispatch(StartEtasRequestStatus());
    return client.getAllArrivalTimes().then(
      (etas: ArrivalTime[]) => {
        dispatch(AddAllEtas(etas));
        dispatch(EndEtasRequestStatus());
      },
      message => {
        dispatch(EndEtasRequestStatus());
        alert("Error feching Arrival Times" + message);
      }
    );
  };
}
