import { AnyAction } from 'redux';
import { ThunkAction, ThunkDispatch } from 'redux-thunk';
import { ArrivalTimeService, Route } from '../services/ArrivalTimeService';

export enum ActionTypes {
  ADD_ALL_ROUTES = "ADD_ALL_ROUTES",
  ROUTES_REQUEST_STATUS = "ROUTES_REQUEST_STATUS",
  ADD_ERROR_FETCHING_ROUTES = "ADD_ERROR_FETCHING_ROUTES"
}

export interface AddAllRoutesAction {
  type: ActionTypes.ADD_ALL_ROUTES;
  payload: { routes: Route[] };
}
export interface RoutesRequestStatusAction {
  type: ActionTypes.ROUTES_REQUEST_STATUS;
  payload: { isFeching: boolean };
}
export interface AddErrorFetchingRoutesAction {
  type: ActionTypes.ADD_ERROR_FETCHING_ROUTES;
  payload: { isError: boolean; message: string };
}

export function AddAllRoutes(routes: Route[]): AddAllRoutesAction {
  return {
    type: ActionTypes.ADD_ALL_ROUTES,
    payload: { routes: routes }
  };
}

export function StartRoutesRequestStatus(): RoutesRequestStatusAction {
  return {
    type: ActionTypes.ROUTES_REQUEST_STATUS,
    payload: { isFeching: true }
  };
}

export function EndRoutesRequestStatus(): RoutesRequestStatusAction {
  return {
    type: ActionTypes.ROUTES_REQUEST_STATUS,
    payload: { isFeching: false }
  };
}

export function AddErrorOnRoutesRequest(
  isError: boolean,
  message: string
): AddErrorFetchingRoutesAction {
  return {
    type: ActionTypes.ADD_ERROR_FETCHING_ROUTES,
    payload: { isError: isError, message: message }
  };
}

export type Action =
  | AddAllRoutesAction
  | RoutesRequestStatusAction
  | AddErrorFetchingRoutesAction;

export interface State {
  routes: Route[];
  isFeching: boolean;
  isErrorFetching: boolean;
  errorMessage: string;
}

export const initialState: State = {
  routes: [],
  isFeching: false,
  isErrorFetching:false,
  errorMessage:''

};

export function reducer(state: State = initialState, action: Action): State {
  switch (action.type) {
    case ActionTypes.ADD_ALL_ROUTES: {
      return {
        ...state,
        routes: action.payload.routes
      };
    }
    case ActionTypes.ROUTES_REQUEST_STATUS: {
      return {
        ...state,
        isFeching: action.payload.isFeching
      };
    }
    case ActionTypes.ADD_ERROR_FETCHING_ROUTES: {
        return {
          ...state,
          isErrorFetching: action.payload.isError,
          errorMessage:action.payload.message
        };
      }
    default: {
      return state;
    }
  }
}

const client = new ArrivalTimeService();

export function getAllRoutes(): ThunkAction<Promise<void>, {}, {}, AnyAction> {
  return (dispatch: ThunkDispatch<{}, {}, AnyAction>): Promise<void> => {
    dispatch(StartRoutesRequestStatus());
    return client.getAllRoutes().then(
      (routes: Route[]) => {
        dispatch(AddAllRoutes(routes));
        dispatch(EndRoutesRequestStatus());
      },
      message => {
        dispatch(EndRoutesRequestStatus());
        alert("Error feching routes" + message);
      }
    );
  };
}
