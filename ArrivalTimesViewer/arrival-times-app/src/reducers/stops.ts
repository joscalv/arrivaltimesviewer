import { Stop, ArrivalTimeService } from "../services/ArrivalTimeService";
import { ThunkAction, ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";

export enum ActionTypes {
  ADD_ALL_STOPS = "ADD_ALL_STOPS",
  STOPS_REQUEST_STATUS = "STOPS_REQUEST_STATUS"
}

export interface AddAllStopsAction {
  type: ActionTypes.ADD_ALL_STOPS;
  payload: { stops: Stop[] };
}
export interface StopsRequestStatusAction {
  type: ActionTypes.STOPS_REQUEST_STATUS;
  payload: { isFeching: boolean };
}

export function AddAllStops(stops: Stop[]): AddAllStopsAction {
  return {
    type: ActionTypes.ADD_ALL_STOPS,
    payload: { stops: stops }
  };
}

export function StartStopRequestStatus(): StopsRequestStatusAction {
  return {
    type: ActionTypes.STOPS_REQUEST_STATUS,
    payload: { isFeching: true }
  };
}

export function EndStopRequestStatus(): StopsRequestStatusAction {
  return {
    type: ActionTypes.STOPS_REQUEST_STATUS,
    payload: { isFeching: false }
  };
}

export type Action = AddAllStopsAction | StopsRequestStatusAction;

export interface State {
  stops: Stop[];
  isFeching: boolean;
}

export const initialState: State = {
  stops: [{ id: 1, name: "Stop1" }],
  isFeching: false
};

export function reducer(state: State = initialState, action: Action): State {
  switch (action.type) {
    case ActionTypes.ADD_ALL_STOPS: {
      return {
        ...state,
        stops: action.payload.stops
      };
    }
    case ActionTypes.STOPS_REQUEST_STATUS: {
      return {
        ...state,
        isFeching: action.payload.isFeching
      };
    }
    default: {
      return state;
    }
  }
}

const client = new ArrivalTimeService();

export function getAllStops(): ThunkAction<Promise<void>, {}, {}, AnyAction> {
  return (dispatch: ThunkDispatch<{}, {}, AnyAction>): Promise<void> => {
    dispatch(StartStopRequestStatus());
    return client.getAllStops().then(
      (stops: Stop[]) => {
        dispatch(AddAllStops(stops));
        dispatch(EndStopRequestStatus());
      },
      message => {
        dispatch(EndStopRequestStatus());
        alert("Error feching stops" + message);
      }
    );
  };
}
