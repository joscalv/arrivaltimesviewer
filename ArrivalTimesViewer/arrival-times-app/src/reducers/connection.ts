import { Action } from 'redux';

export enum ActionTypes {
  CONNECT = "CONNECT",
  CONNECTED = "CONNECTED",
  DISCONNECT = "DISCONNECT"
}

export interface ConnectAction {
  type: ActionTypes.CONNECT;
}

export interface ConnectedAction {
  type: ActionTypes.CONNECTED;
}

export interface DisconnectAction {
  type: ActionTypes.DISCONNECT;
}

export function connectToHub(): ConnectAction {
  return {
    type: ActionTypes.CONNECT
  };
}

export function connectedToHub(): ConnectedAction {
  return {
    type: ActionTypes.CONNECTED
  };
}

export function disconnectToHub(): DisconnectAction {
  return {
    type: ActionTypes.DISCONNECT
  };
}

export type Action = ConnectAction | DisconnectAction;

export interface State {
  isConnected: boolean;
}

export const initialState: State = {
  isConnected: false
};

export function reducer(state: State = initialState, action: Action) {
  switch (action.type) {
    case ActionTypes.CONNECT:
      return { ...state, isConnected: false };
    case ActionTypes.CONNECTED:
      return { ...state, isConnected: true };
    case ActionTypes.DISCONNECT:
      return { ...state, isConnected: false };
    default:
      return state;
  }
}
