import { combineReducers } from 'redux';
import * as arrivalTimesReducer from './arrivalTimes';
import * as routesReducer from './routes';
import * as stopsReducer from './stops';

export interface State {
  stops: stopsReducer.State;
  routes: routesReducer.State;
  arrivalTimes: arrivalTimesReducer.State;
}

export const initialState: State = {
  stops: stopsReducer.initialState,
  routes: routesReducer.initialState,
  arrivalTimes: arrivalTimesReducer.initialState
};

export const reducer = combineReducers<State>({
  stops: stopsReducer.reducer,
  routes: routesReducer.reducer,
  arrivalTimes: arrivalTimesReducer.reducer
});
