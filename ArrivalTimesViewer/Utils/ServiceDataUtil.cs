﻿using System;
using System.Collections.Generic;
using System.Linq;
using ArrivalTimesViewer.Dto;
using Microsoft.AspNetCore.Routing;

namespace ArrivalTimesViewer.Utils
{
    public static class ServiceDataUtil
    {
        public static List<StopDto> Stops { get; } = new List<StopDto>()
        {
            new StopDto() {Id = 0, Name = "Le Conte / Broxton", Description = "900 S Broxton Ave, Los Angeles"},
            new StopDto() {Id = 1, Name = "Le Conte / Westwood", Description = "900 Westwood Boulevard, Los Angeles"},
            new StopDto() {Id = 2, Name = "Le Conte Eb & Tiverton Ns", Description = "10833 Le Conte Ave, Los Angeles"},
            new StopDto() {Id = 3, Name = "Hilgard Nb & Le Conte Fs", Description = "886 Hilgard Ave, Los Angeles"},
            new StopDto() {Id = 4, Name = "Hilgard Nb & Manning Ave Ns", Description = "800 S Hilgard Ave, Los Angeles"},
            new StopDto() {Id = 5, Name = "Hilgard Nb & Westholme Ns", Description = "Hilgard Avenue, Los Angeles"},
            new StopDto() {Id = 6, Name = "Hilgard / Wyton", Description = "372 S Hilgard Ave, Los Angeles"},
            new StopDto() {Id = 7, Name = "Hilgard / Charing Cross", Description = "10501 W Charing Cross Road, Los Angeles"},
            new StopDto() {Id = 8, Name = "Sunset / Beverly Glen", Description = "10400 W Sunset Blvd, Los Angeles"},
            new StopDto() {Id = 9, Name = "Sunset / Mapleton", Description = "10281 W Sunset Blvd, Los Angeles"},
        };

        public static List<RouteDto> Routes { get; } = new List<RouteDto>()
        {
            new RouteDto() { Id = 0,Name = "Route 1" },
            new RouteDto() { Id = 1,Name = "Route 2" },
            new RouteDto() { Id = 2,Name = "Route 3" }
        };

        public static List<ArrivalTimeDto> CalculateArrivalTimes(DateTime dateTime)
        {
            int currentMinuteInPeriod = dateTime.Minute % 15;
            return ServiceDataUtil.Stops.SelectMany(s =>
            {
                return Routes.Select(r =>
                    {
                        int eta1 = ((15 + (s.Id * 2) + (r.Id * 2)) - currentMinuteInPeriod) % 15;
                        int eta2 = eta1 + 15;
                        return new ArrivalTimeDto() { StopId = s.Id, RouteId = r.Id, Etas = new List<int> { eta1, eta2 } };
                    }
                );
            }).ToList();
        }

    }


}
