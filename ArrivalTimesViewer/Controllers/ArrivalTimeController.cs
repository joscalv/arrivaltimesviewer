﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ArrivalTimesViewer.Dto;
using ArrivalTimesViewer.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ArrivalTimesViewer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArrivalTimeController : ControllerBase
    {
        private readonly ILogger<ArrivalTimeController> _logger;

        public ArrivalTimeController(ILogger<ArrivalTimeController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(List<ArrivalTimeDto>))]
        [Produces("application/json")]
        public IEnumerable<ArrivalTimeDto> GetArrivalTimes()
        {
            return ServiceDataUtil.CalculateArrivalTimes(DateTime.Now);

        }


        [HttpGet]
        [Route("api/stop/{stopId}/[controller]")]
        [ProducesResponseType(200, Type = typeof(List<ArrivalTimeDto>))]
        [Produces("application/json")]
        public IEnumerable<ArrivalTimeDto> GetArrivalTime(int stopId)
        {
            return ServiceDataUtil.CalculateArrivalTimes(DateTime.Now).Where(eta => eta.StopId == stopId);
        }


    }
}
