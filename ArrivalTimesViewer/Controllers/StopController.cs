﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ArrivalTimesViewer.Dto;
using ArrivalTimesViewer.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ArrivalTimesViewer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StopController : ControllerBase
    {
        private readonly ILogger<StopController> _logger;

        public StopController(ILogger<StopController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(List<StopDto>))]
        [Produces("application/json")]
        public IEnumerable<StopDto> GetStops()
        {
            return ServiceDataUtil.Stops;
        }

        [HttpGet("{id}")]
        [ProducesResponseType(200, Type = typeof(StopDto))]
        [Produces("application/json")]
        public StopDto GetStop(int id)
        {
            return ServiceDataUtil.Stops.FirstOrDefault(stop => stop.Id == id);
        }


        
    }
}
