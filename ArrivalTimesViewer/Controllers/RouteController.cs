﻿using System.Collections;
using System.Collections.Generic;
using ArrivalTimesViewer.Dto;
using ArrivalTimesViewer.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ArrivalTimesViewer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RouteController : ControllerBase
    {
        private readonly ILogger<RouteController> _logger;

        public RouteController(ILogger<RouteController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(List<RouteDto>))]
        [Produces("application/json")]
        public IEnumerable<RouteDto> GetRoutes()
        {
            return ServiceDataUtil.Routes;
        }


    }
}
