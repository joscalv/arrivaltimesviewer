﻿using System;
using System.Threading;
using System.Threading.Tasks;
using ArrivalTimesViewer.Hubs;
using ArrivalTimesViewer.Utils;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace ArrivalTimesViewer.HostedServices
{
    public class AutoUpdateArrivalTimes : IHostedService, IDisposable
    {
        private Timer _timerUpdateArrivalTimes;
        private readonly ILogger<AutoUpdateArrivalTimes> _logger;
        private readonly IHubContext<ArrivalTimesHub> _hubContext;

        public AutoUpdateArrivalTimes(ILogger<AutoUpdateArrivalTimes> logger, IHubContext<ArrivalTimesHub> hubContext)
        {
            _logger = logger;
            _hubContext = hubContext;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _timerUpdateArrivalTimes = new Timer(UpdateArrivalTimes, null, TimeSpan.Zero, TimeSpan.FromSeconds(60));
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _timerUpdateArrivalTimes?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            _timerUpdateArrivalTimes?.Dispose();
        }

        private void UpdateArrivalTimes(object state)
        {
            _logger.LogInformation("Generate new Arrival times and send to clients");
            var etas = ServiceDataUtil.CalculateArrivalTimes(DateTime.Now);
            _hubContext.Clients.All.SendAsync("UpdateArrivalTimes", etas);
        }
    }
}